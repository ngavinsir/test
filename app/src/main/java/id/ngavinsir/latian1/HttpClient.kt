package id.ngavinsir.latian1

import io.reactivex.Single
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Request

class HttpClient {

    companion object {
        val instance = HttpClient()
    }

    fun getPosts(): Single<String> {
        return Single.create { e ->
            val client = OkHttpClient()
            val request = Request.Builder()
                .url("https://jsonplaceholder.typicode.com/posts")
                .build()
            val response = client.newCall(request).execute()
            if(response.isSuccessful) e.onSuccess(response.body()?.string()!!)
        }
    }

}