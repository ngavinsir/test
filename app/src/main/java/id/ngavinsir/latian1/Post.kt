package id.ngavinsir.latian1

data class Post(
    val id: Int,
    val title: String,
    val body: String
)